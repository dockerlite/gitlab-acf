# Git avec le remote repository

## Configurer gitlab 
Allez dans votre navigateur sous votre compte gitlab   
creer un nouveau blank project esrf sans readme et sans d'autre fichier 

## Push vers le remote gitlab projet
```shell
git remote -v  # check 
git config --global user.name "votre nom"
git config --global user.email "email "
```
## Installation de la cle SSH
changer c:/windows/window32/drivers/hosts  
entre l'adresse ip et gitlab-ce
```shell
git remote -v
git remote add origin ssh://git@gitlab-ce:32222/teacher/esrf.git
git remote -v
ssh-keygen -t rsa
# entrer le nam de la directory et le nom de la cle  /c/gitlab-course/key
cd ..
ll
cat key.pub
# copier cette cle dans gitlab 
cd esrf/
GIT_SSH_COMMAND='ssh -i ../key' git push -u --all origin
```

## Commandes de base 
git log --oneline --all 

## creez autre user 
passer en root  sous gitlab   
Dans  admin area 
creez un user paul  

## Merge request
paul fait un fork du projet  
git clone dans une autre directorie
paul cree un fichier geodesie.txt dans le main
```shell
git remote set-url origin ssh://git@gitlab-ce:32222/<current_user>/esrf.git 
git remote -v 
git remote add upstream  ssh://git@gitlab-ce:32222/<origin_user>/esrf.git
git pull upstream master  # check si l'origine user a modifier le master
```
![merge-request](screenshots/merge-request.png)

#### simulation de plusieurs users

![new-merge-request](screenshots/new-merge-request.png)

![adding-merge-request](screenshots/adding-merge-request.png)


GIT_SSH_COMMAND='ssh -i ../key' git push -u --all origin

