# Git merge strategie

## Pre-requis 
importer le projet calc2, lien fourni     
git clone https://github.com/brentlaster/calc2.git en local


## Revision des commandes de base pour un nouveau projet
```cd ~/calc2 && git branch```

### Check all branches
```shell
git branch -a
### Check only remote branches
git branch -r
### Check all branches with their associated commit messages
git branch -av
### Get the number of commit a branch is ahead from the master
git rev-list --left-right --count main...remotes/origin/exp
## Edit the list of all commits ahead from the master for a specific branch 
git rev-list --left-right --pretty=oneline main...remotes/origin/exp
#display 
git log --all --decorate --oneline --graph
#Create a new branch local 
git branch mybranch 
#switch to the branch 
git checkout mybranch
#or your can one command
git checkout -b mytest
#Create a branch from stash command
#modify calc.html 
#vi calc.html
git stash save "test"
git stash list
git stash branch my-modify-stash stash@{0}
``` 
### Exemple de cherry-pick
```shell
git clone http://gitlab-ce/paul/calc2.git other
cd  other/
git branch
git branch features origin/features
git log --oneline features
git branch
git checkout -b cpick
git branch
git config --global user.email "paul@gmail.com"
git config --global user.name "paul"
git log --oneline features
git cherry-pick d003b91
```
![cherry-pick-calc](screenshots/cherry-pick.png)

```shell
git log cpick --oneline
git log --all --decorate --oneline --graph 
```
## Cas de rebase 
Rester dans la branche cpick  
```shell
git rebase 3753e5a
git log --all --decorate --oneline --graph
git switch main
git merge cpick
git log --all --decorate --oneline --graph
#git push
GIT_SSH_COMMAND='ssh -i ../key' git push -u origin main  
```
![rebase-calc](screenshots/rebase.png)

### Rebase strategie
```shell
cd /c/project && cd calc2
git log --oneline
git branch -av
git branch ui origin/ui
git log --oneline
git log --oneline ui
````

![ui](screenshots/calc2-ui.png)

```shell
git merge ui 
#git push origin main 
GIT_SSH_COMMAND='ssh -i ../key' git push -u origin main
```
![push_conflicted](screenshots/conflicted-push.png)

```shell
git pull origin main
git merge --abort
git log --oneline
git fetch 
git log --oneline origin/main
git rebase origin/main
git rebase --abort
git rebase -Xours origin/main
git log --oneline
```
![graph-xours](screenshots/graph-xours.png)
```shell
#git push origin main 
GIT_SSH_COMMAND='ssh -i ../key' git push -u origin main
```