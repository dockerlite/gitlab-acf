# Git merge conflits 


## Simple merge conflits
Se placer dans le repository initial  
Merge la branche helen dans le master 

![merge-init](screenshots/merge-init.png)
```shell
git switch master
git merge helen
cat geodesie.txt
git remote -v
git log --oneline
git switch master
git log --oneline
git push
git pull
vi geodesie.txt
git status
git add geodesie.txt
git commit -m "merge"
git pull
git log --oneline
git log
git push
git log  --oneline
```
![merge-done](screenshots/merge-done.png)

## Merge de plusieurs fichiers 
```shell
cd c:
cd project
git clone https://git.eclipse.org/r/jgit/jgit
#The purpose of the “git reset” command is to move the current HEAD to the commit specified
git checkout master && git reset --hard b14a939
git config --global alias.co checkout
git config --global alias.br branch  
git config --global alias.ci commit  
git config --global alias.st status  
git config --global core.editor "vi"
git st
git config --global alias.unstage 'reset HEAD --'
git config --global alias.ll 'log --pretty=format:"%C(yellow)%h%Cred%d%Creset%s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --numstat'
git ll 
# edit ~/.gitconfig
## add
editconflicted= "!f() { git ls-files --unmerged | cut -f2 | sort -u ; } ; f | xargs -o $EDITOR"
```

### creer 2 branches,avec une tentative de merge 
```shell
git branch A 03f78fc
git branch B 9891497
git checkout A
git merge B
export EDITOR=vim
git editconflicted
```
