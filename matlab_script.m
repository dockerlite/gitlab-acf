
x = [114.20, 141.55,166.23,157.62,148.27,118.75]
y = [152.00, 162.97,149.16,128.47,108.69,110.34]
z = [257.30, 261.89,262.44,258.08,253.12,248.17]

 xlin = linspace(min(x), max(x), 100);
 ylin = linspace(min(y), max(y), 100);
 [X,Y] = meshgrid(xlin, ylin);

 Z = griddata(x,y,z,X,Y,'natural');
 mesh(X,Y,Z)
 axis tight; hold on

 plot3(x,y,z,'.','MarkerSize',15)

 xyzPoints =