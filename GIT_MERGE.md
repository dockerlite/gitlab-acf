## Git merge, rebase, cherry-pick 

### Creer une branche, ajout 2 commits et merge en fast-forward
```shell 
git checkout -b laser1
vi laser1.txt
#add pi = 3.14
git add .
git commit -m"add pi"
git log --oneline --graph
vi mesure.txt
## add meter = 5
git add . 
git commit -m"add meter" 
git log --oneline --graph
git checkout master 
git log --oneline --graph
git merge laser1 
git log --oneline --graph
```
![ff](screenshots/fast-forward.png)

### Squash commit 
```shell
 git checkout -b laser2
 vi lidar.txt
 git add lidar.txt
 # add  1 , x , y , z
 git commit -m " add lidar"
 git log --oneline
 vi lidar.txt
 # add  2 , x , y , z
 git commit -am " add lidar"
 vi lidar.txt
 # add 3 ,x ,y ,z
 git commit -am " add lidar"
 git log --oneline
 ```
 #### squash les 2 derniers commits dans le premier
```shell
 git reset --soft HEAD~2
 git log --oneline --all
 
 cat lidar.txt
```
## Rebase 
```shell
vi lidar.txt
# add 4 ,x ,y ,z
git commit -am " add lidar"
git log --oneline
git checkout master
git log --oneline --all
git rebase laser2
```

## Cherry-pick 
```shell
git checkout -b  alain
git log --oneline --all
vi geodesie.txt
git add geodesie.txt
git commit -m "add alain geodesie"
vi polygonal.txt
git add polygonal.txt
git commit -m "add alain nodal point"
git log --oneline --all
git checkout master
git log --oneline --all
git checkout -b helen
git log --oneline --all
git log --oneline --all --graph
vi rtk.txt
git add rtk.txt
git commit -m "add frequence"
vi rtk.txt
git commit -am "add osr"
git log --oneline --all
git cherry-pick -x <sha-1> <sha-1> 
git log --oneline --all

```

## Stash 
```shell
vi rtk.txt 
git commit -am "helen a include geodesie/nodal"
vi rtk.txt
## add travail en cours 
git status
git stash save "helen job in progress"
git status
cat rtk.txt
```
### manage stash
```shell
git stash list
git stash branch my-modify-stash stash@{0} # cree une branche de stash
``` 



