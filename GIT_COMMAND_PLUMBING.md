# git commandes plumbing 

### TP
```shell
mkdir plumbing  && cd plumbing && git init

#List all files/directories in the .git directory
ls .git

#Usage of a git plumbing command
echo " an awesome sheep admires the Alps" > animals.txt
git hash-object -w animals.txt

#The hash-object command takes a path to a file, reads its contents, and saves the contents of the file to the Git object store. 
#It returns a hex string – the ID of the object it’s just created.

find .git/objects -type f

# Get the content of a file using the SHA1 index
git cat-file -p <SHA1>

#In a porcelain Git workflow, you add files to the index with **git add**, then take a snapshot of the index with git commit. 
#With plumbing, there are several extra steps

git update-index --add animals.txt
ls .git

#We can see what we’ve added to the index or the staging area,  with the plumbing command ls-files
git ls-files

Alternatively, the porcelain command status gives a more verbose view of the index:
git status
```
 
