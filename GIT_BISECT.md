# Git bisect
Another useful tool that Git provides is bisect. The bisect command
provides a mechanism for quickly locating where a problem or change
was introduced in a range of commits.

git clone https://github.com/brentlaster/bisect2.git  && cd bisect2

Set the range of commits to test
Here, HEAD represents a known bad revision and HEAD˜10
represents a known good revision—ten commits before current HEAD.
This establishes your starting range and starts the operation in one
command.

## usage of git bisect 
```shell
git bisect start HEAD HEAD~10
git log --oneline
# check 
./sum2.sh 5 4
# This is a bad result
git bisect bad
git log --oneline
# check 
./sum2.sh 5 4
git bisect good
git log --oneline
./sum2.sh 5 4
git bisect good
git log --oneline
#Stop using bisect 
git bisect reset HEAD
# eventually create a branch 
git checkout -b dev 
git log --oneline
```